function readJSON(file) {
    const request = new XMLHttpRequest();
    request.open('GET', file, false);
    request.send(null);
    if (request.status == 200)
        return request.responseText;
};

const productsJSON = readJSON('products.json'); //file
const products = JSON.parse(productsJSON); //object
let PRICE = 0;

function init(chooseColor) {
    
    $.each(products, function (index, obj) {
        function clickPhone() {
            $('#' + obj.id).click(function () {
                transition('#choose-iphone', '#choose-color');
                PRICE += obj.price;
                chooseColor(obj);
            })
        }
        clickPhone()
    })
}

//controller
(function () {

    const chooseColor = function (iPhoneModel) {
        generateCapacitySection(iPhoneModel);
        generateColorSection(iPhoneModel);

        $.each(iPhoneModel.options[0].values, function (index, obj) {
            function clickColor(iPhoneModel) {
                $('#' + obj.id).click(function () {
                    transition('#choose-color', '#choose-capacity')
                    PRICE += obj.priceModifier;
                    chooseCapacity(iPhoneModel, obj);
                })
            }
            clickColor(iPhoneModel)
        })
    }

    const chooseCapacity = function (iPhoneModel, color) {
        $.each(iPhoneModel.options[1].values, function (index, obj) {
            function clickCapacity(iPhoneModel, color) {
                $('#' + obj.id).click(function () {
                    transition('#choose-capacity', '#form');
                    PRICE += obj.priceModifier;
                    onSubmitClick(iPhoneModel, color, obj);
                })
            }
            clickCapacity(iPhoneModel, color)
        })
    }

    const onSubmitClick = function (model, color, capacity) {
        $('form').submit(function (event) {
            event.preventDefault();
            const name = $('#name').val();
            const surname = $('#surname').val();
            const email = $('#email').val();
            const street = $('#street').val();
            const houseNumber = $('#houseNumber').val();
            const city = $('#city').val();
            const postCode = $('#postCode').val();
            const summary = {
                user: {
                    name: name,
                    surname: surname,
                    email: email,
                    address: {
                        street: street,
                        houseNumber: houseNumber,
                        city: city,
                        postcode: postCode
                    }
                },
                product: {
                    id: model.id,
                    options: [
                        {
                            id: model.options[0].id,
                            value: color.id
                        },
                        {
                            id: model.options[1].id,
                            value: capacity.id
                        }
                    ],
                    amount: PRICE
                }
            }
            console.info({
                summary
            });

            transition('#form', '#summary');

            $('#iPhoneSummary').text('Congratulations! You just bought a ' + color.name + ' ' + model.name + ' with ' + capacity.name + ' of capacity!');
            $('#iPhonePrice').text('Price of your model: ' + PRICE + '$');

        })
    }

    generatePhoneSection();

    init(chooseColor);
})()

function confirmEmail() {
    const email = $('#email').val();
    const confemail = $('#validateEmail').val();

    if (email != confemail) {
        alert('Email Not Matching!');
        $('#validateEmail').val('');
    };
}

function transition(chosenToFade, chosenToAppear) {
    $(chosenToFade).css('animation', 'fadeOut');
    $(chosenToFade).css('animation-duration', '0.5s');
    $(chosenToFade).css('animation-fill-mode', 'forwards');

    setTimeout(function () {
        $(chosenToFade).css('display', 'none');
        $(chosenToAppear).css('display', 'block');
    }, 500)
}

function menuWrap() {
    var x = document.getElementById('id-main-menu');
    if (x.className === 'main-menu') {
        x.className += ' responsive';
    } else {
        x.className = 'main-menu';
    }
}

function generateCapacitySection(iPhoneModel) {
    $.each(iPhoneModel.options[1].values, function (index, obj) {
        $('.capacity-area').append(
            `<a id="${obj.id}">
                <div class="capacity-product">
                    <div class="capacity-product-title">
                        ${obj.name}
                    </div>
                    <div class="capacity-product-price">
                       +${obj.priceModifier}$
                    </div>
                </div>
            </a>`
        )
    })
}

function generateColorSection(iPhoneModel) {
    $.each(iPhoneModel.options[0].values, function (index, obj) {
        $('.color-flex').append(
            `<a id="${obj.id}">
                <div class="color-image-zone" style="${obj.colorStyle}">
                    <div class="color-image">
                        <div class="color-image-back">
                            <img src="${obj.imgBack}">
                        </div>
                        <div class="color-image-front">
                            <img src="${obj.imgFront}">
                        </div>
                    </div>
                </div>
                <div class="color-description">
                    ${obj.name}
                </div>
                <div class="color-price">
                    +${obj.priceModifier}$
                </div>
            </a>`
        )
    })
}

function generatePhoneSection() {
    $.each(products, function (index, obj) {
        $('.shopping-area').append(
            `<a id="${obj.id}" class="chosenIphone">
                <div class="shopping-product" style="${obj.shoppingStyle}">
                    <div class="shopping-product-image">
                        <img src="${obj.image}">
                    </div>
                    <div class="shopping-product-title">
                        ${obj.name}
                    </div>
                    <div class="shopping-product-price">
                        <del>${obj.oldPrice}</del> <ins>${obj.price}</ins>
                    </div>
                </div>
            </a>`
        )
    })
}